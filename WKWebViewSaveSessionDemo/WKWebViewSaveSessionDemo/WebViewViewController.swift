//
//  WebViewViewController.swift
//  WKWebViewSaveSessionDemo
//
//  Created by smartSense on 01/10/19.
//  Copyright © 2019 smartSense. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        // Do any additional setup after loading the view.
        
        guard let url = URL(string: cueUrl.dashboard) else { return }
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }

}

struct cueUrl {
//    static let dashboard = "http://34.205.107.109:7000/user/dashboard"
    static let dashboard = "https://dev-patient.kevahealth.com/enterprise/abc123"
}

