//
//  HomeViewController.swift
//  WKWebViewSaveSessionDemo
//
//  Created by smartSense on 01/10/19.
//  Copyright © 2019 smartSense. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func continueWithKevaHealthActionListener(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var destView:UIViewController!
        if #available(iOS 13.0, *) {
            destView = storyboard.instantiateViewController(identifier: "WebViewViewController")
        } else {
            destView = storyboard.instantiateViewController(withIdentifier: "WebViewViewController")
        }
        self.navigationController?.pushViewController(destView, animated: true)
    }


}
